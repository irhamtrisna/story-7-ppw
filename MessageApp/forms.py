from django import forms
from .models import Message, Name

class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['message']

    def customSave(self):
        lv = self.save(commit=False)
        lv.save()
        return lv

class NameForm(forms.ModelForm):
    class Meta:
        model = Name
        fields = ['name']