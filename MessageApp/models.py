from django.db import models

# Create your models here.

class Name(models.Model):
    name = models.CharField(max_length=50)

class Message(models.Model):
    message = models.TextField()
    hasMessage = models.ForeignKey(to=Name, on_delete=models.CASCADE, null=True, blank=True)