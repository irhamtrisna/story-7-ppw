from django.urls import path
from . import views

app_name = 'MessageApp'

urlpatterns = [
    path('message/', views.index, name='index'),
    path('confirmation/<int:ids>/', views.confirmation, name='confirmation'),
]