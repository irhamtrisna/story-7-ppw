from django.shortcuts import render, redirect, get_object_or_404
from .models import Message, Name
from .forms import MessageForm, NameForm

# Create your views here.
def index(request):
    names = Name.objects.all()
    messages = Message.objects.all()
    loop_times = range(0, messages.count())
    if request.method == "POST":
        nameForm = NameForm(request.POST)
        messageForm = MessageForm(request.POST)

        if nameForm.is_valid() and messageForm.is_valid():
            nameForm.save()
            inst = messageForm.customSave()
            return redirect('MessageApp:confirmation', ids = inst.id)

    else:
        nameForm = NameForm()
        messageForm = MessageForm()

    return render(request, 'main.html', {'messageForm':messageForm, 'nameForm':nameForm, 'loop_times':loop_times, 'zip_list':zip(names, messages)})

def confirmation(request, ids):
    to_confirm = get_object_or_404(Message, id = ids)
    names = Name.objects.all()
    name_to_confirm = names[names.count()-1]

    if request.method == 'POST':
        to_confirm.delete()
        name_to_confirm.delete()
        return redirect('MessageApp:index')

    return render(request, 'confirmation.html', {'to_confirm':to_confirm})