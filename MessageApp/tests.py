from django.test import TestCase, Client
from django.urls import resolve
from .models import Message, Name
from .views import index, confirmation
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import unittest

# Create your tests here.

class TestAddMessage(TestCase):
    def test_message_model_create_new_object(self):
        new_name = Name(name="Irham")
        new_name.save()
        new_message = Message(message="Hello World")
        new_message.save()
        self.assertEqual(Message.objects.all().count(), 1)
        self.assertEqual(Name.objects.all().count(), 1)

class TestMessage(TestCase):
    def test_message_url_is_exist(self):
        response = Client().get('/message/')
        self.assertEqual(response.status_code, 200)

    def test_message_url_POST_is_exist(self):
        response = Client().post('/message/', data={'name':'Irham','message':'Hello World'})
        self.assertEqual(response.status_code, 302)

    def test_message_using_index_func(self):
        found = resolve('/message/')
        self.assertEqual(found.func, index)

    def test_message_using_main_html(self):
        response = Client().get('/message/')
        self.assertTemplateUsed(response, 'main.html')

class TestConfirmMessage(TestCase):
    def setUp(self):
        message = Message(message="Hello world")
        name = Name(name="Irham")
        message.save()
        name.save()

    def test_confirm_url_post_is_exist(self):
        response = Client().post('/confirmation/1/')
        self.assertEqual(response.status_code, 302)

    def test_confirm_url_is_exist(self):
        response = Client().get('/confirmation/1/')
        self.assertEqual(response.status_code, 200)

class FunctionalTest(TestCase):
    def setUP(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def checkPageTitle_and_testAddMessage(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/message')
        self.assertIn('Document', selenium.title)

        selenium.get('http://localhost:8000/message')

        selenium.implicitly_wait(5)

        name = selenium.find_element_by_id('id_name')
        message = selenium.find_element_by_id('id_message')
        name.send_keys('Irham')
        message.send_keys('Hello World!')

        save = selenium.find_element_by_name('save-button')
        save.click()
        selenium.implicitly_wait(5)

        confirm = selenium.find_element_by_id('confirm-button')
        confirm.click()

        self.assertIn('Irham', selenium.page_source)
        self.assertIn('Hello World!', selenium.page_source)

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()